Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0008-background-light-support-s-bl).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |[de](https://gitlab.com/ourplant.net/products/s3-0008-background-light-support-s-bl/-/raw/main/01_operating_manual/S3-0008_A_Betriebsanleitung.pdf)|
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0008-background-light-support-s-bl/-/raw/main/02_assembly_drawing/s3-0008-A_backr-light-support.PDF)|
| circuit diagram          |[de](https://gitlab.com/ourplant.net/products/s3-0008-background-light-support-s-bl/-/raw/main/03_circuit_diagram/S3-0008-EPLAN-A.pdf)|
| maintenance instructions |                  |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0008-background-light-support-s-bl/-/raw/main/05_spare_parts/S3-0008-EVL-A.pdf)|

